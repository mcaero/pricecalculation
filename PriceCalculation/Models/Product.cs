﻿namespace Models
{
    public class Product
    {
        public ProductType ProductType { get; set; }

        public string Name { get; set; }

        public decimal Cost { get; set; }
    }
}
