﻿namespace Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class Basket : IPriceCalculator
    {

        public List<ProductLine> ProductList { get; set; }

        public decimal CalculatePrice()
        {
            
            decimal total = 0;
            foreach (var product in ProductList)
            {
                total = total + (product.Quantity * product.Product.Cost);
            }
            return total;
        }

        public List<ProductLine> GetProductLines()
        {
            return ProductList;
        }
       // public BasketOfferDecorator  Offers{ get; set; }


        public void AddProduct(Product Product)
        {
            var productLine = ProductList.FirstOrDefault(x => x.Product.ProductType == Product.ProductType);
            if(productLine != null)
            {
                productLine.Quantity++;
            }else
            {
                var newProductLine = new ProductLine()
                {
                    Product = Product,
                    Quantity = 1
                };
                ProductList.Add(newProductLine);
            }

        }



    }
}
