﻿namespace Contracts
{
    public interface IPriceCalculator
    {
       decimal CalculatePrice();

    }
}
