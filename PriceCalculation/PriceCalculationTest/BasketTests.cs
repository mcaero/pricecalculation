﻿namespace PriceCalculationTest
{
    using Models;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Collections.Generic;

    [TestClass]
    public class BasketTests
    {

        private Product Butter = new Product()
        {
            Cost = 0.80m,
            Name = "Butter",
            ProductType = ProductType.Butter
        };
        private Product Milk = new Product()
        {
            Cost = 1.15m,
            Name = "Milk",
            ProductType = ProductType.Milk
        };
        private Product Bread = new Product()
        {
            Cost = 1.00m,
            Name = "Bread",
            ProductType = ProductType.Bread
        };

        
        //private Offers newOffer = new Offers();

        [TestMethod]
        public void Basket_CalculatePrice_WithoutOffers()
        {      
                  
            ProductLine lineBread = new ProductLine()
            {
                Product = Bread,
                Quantity = 1
            };
            ProductLine lineMilk = new ProductLine()
            {
                Product = Milk,
                Quantity = 1
            };
            ProductLine lineButter = new ProductLine()
            {
                Product = Butter,
                Quantity = 1
            };

            List<ProductLine> listProducts = new List<ProductLine>();
            listProducts.Add(lineButter);
            listProducts.Add(lineBread);
            listProducts.Add(lineMilk);
            Basket newBasket = new Basket()
            {
                ProductList = listProducts,
                //Offers = newOffer
            };


            Assert.AreEqual(newBasket.CalculatePrice(), 2.95m);
            
        }

        [TestMethod]
        public void Basket_CalculatePrice_BreadOffer()
        {

            ProductLine lineBread = new ProductLine()
            {
                Product = Bread,
                Quantity = 2
            };
            
            ProductLine lineButter = new ProductLine()
            {
                Product = Butter,
                Quantity = 2
            };

            List<ProductLine> listProducts = new List<ProductLine>();
            listProducts.Add(lineButter);
            listProducts.Add(lineBread);
            Basket newBasket = new Basket()
            {
                ProductList = listProducts,
           
            };
              
            
            BasketOfferDecorator offerBasket = new BasketOfferDecorator(new BasketDecorator(newBasket));

            Assert.AreEqual(3.10m, offerBasket.CalculatePrice());

        }

        [TestMethod]
        public void Basket_CalculatePrice_MilkOffer()
        {

            ProductLine lineMilk = new ProductLine()
            {
                Product = Milk,
                Quantity = 4
            };

            List<ProductLine> listProducts = new List<ProductLine>();
            listProducts.Add(lineMilk);
            Basket newBasket = new Basket()
            {
                ProductList = listProducts,
            };

            BasketOfferDecorator offerBasket = new BasketOfferDecorator(new BasketDecorator(newBasket));
            Assert.AreEqual(offerBasket.CalculatePrice(), 3.45m);

        }

        [TestMethod]
        public void Basket_CalculatePrice_AllOffers()
        {

            ProductLine lineBread = new ProductLine()
            {
                Product = Bread,
                Quantity = 1
            };
            ProductLine lineMilk = new ProductLine()
            {
                Product = Milk,
                Quantity = 8
            };
            ProductLine lineButter = new ProductLine()
            {
                Product = Butter,
                Quantity = 2
            };

            List<ProductLine> listProducts = new List<ProductLine>();
            listProducts.Add(lineButter);
            listProducts.Add(lineBread);
            listProducts.Add(lineMilk);
            Basket newBasket = new Basket()
            {
                ProductList = listProducts,
               
            };



            BasketOfferDecorator offerBasket = new BasketOfferDecorator(new BasketDecorator(newBasket));
            Assert.AreEqual(offerBasket.CalculatePrice(), 9);

        }

        [TestMethod]
        public void Basket_AddProduct()
        {
            //Arrange
            ProductLine milkProductLines = new ProductLine();
            milkProductLines.Product = Milk;
            milkProductLines.Quantity = 2;
            List<ProductLine> productLines = new List<ProductLine>();
            productLines.Add(milkProductLines);
            Basket _basket = new Basket()
            {
                //Offers = newOffer,
                ProductList = productLines
            };

            //Act
            Basket newBasket = new Basket()
            {
                ProductList = new List<ProductLine>(),
                //Offers = newOffer
            };
            newBasket.AddProduct(Milk);
            newBasket.AddProduct(Milk);

            //Assert
            CollectionAssert.AreEqual(_basket.ProductList, newBasket.ProductList);
        }
    }
}
