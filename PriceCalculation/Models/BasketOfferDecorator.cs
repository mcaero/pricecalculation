﻿namespace Models
{
    using System.Collections.Generic;
    using System.Linq;

    public class BasketOfferDecorator :BasketDecorator
    {

        public BasketOfferDecorator(BasketDecorator baseBaket) : base(baseBaket)
        {
            
        }

        public new decimal CalculatePrice()
        {
            return base.CalculatePrice() - CalculateDiscount(GetProductLines());
        }
        private decimal CalculateDiscount(List<ProductLine> ListProductLines)
        {
            decimal discount = 0;
            var milkProductLine = ListProductLines.FirstOrDefault(x => x.Product.ProductType == ProductType.Milk);
            if(milkProductLine != null)
            {
                if(milkProductLine.Quantity >= 4)
                {
                    discount += (milkProductLine.Quantity / 4) * milkProductLine.Product.Cost;
                }
            }
            var butterLine = ListProductLines.FirstOrDefault(x => x.Product.ProductType == ProductType.Butter);
            if(butterLine != null)
            {
                if (butterLine.Quantity >= 2)
                {
                    var breadLine = ListProductLines.FirstOrDefault(x => x.Product.ProductType == ProductType.Bread);
                    if (breadLine != null)
                    {
                        int numberBreadDiscounted = (butterLine.Quantity / 2);
                        if (numberBreadDiscounted >= breadLine.Quantity)
                        {
                            discount += breadLine.Quantity * (breadLine.Product.Cost * 0.5m);
                        }
                        else {
                            discount += numberBreadDiscounted * (breadLine.Product.Cost * 0.5m);
                        }
                        
                        
                        
                    }
                }
            }

            return discount;
        }
    }
}
