﻿namespace Models
{
    public enum ProductType
    {
        Butter, 
        Milk,
        Bread
    }
}
