﻿namespace Models
{
    using System.Collections.Generic;

    public interface IPriceCalculator
    {
        decimal CalculatePrice();

        List<ProductLine> GetProductLines();

        
    }
}
