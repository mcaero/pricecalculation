﻿namespace Models
{
    public class ProductLine
    {
        public Product Product { get; set; }

        public int Quantity { get; set; }

        public override bool Equals(object obj)
        {
            var productLine = obj as ProductLine;

            if (productLine == null)
                return false;

            if (productLine.Quantity != productLine.Quantity)
                return false;

           

            return productLine.Product == Product;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
