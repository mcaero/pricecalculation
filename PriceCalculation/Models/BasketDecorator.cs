﻿namespace Models
{
    using System.Collections.Generic;

    public class BasketDecorator : IPriceCalculator
    {
        IPriceCalculator priceCalculator;
        

        public BasketDecorator(IPriceCalculator PriceCalculator)
        {
            priceCalculator = PriceCalculator;
        }

        public  decimal CalculatePrice()
        {
            return priceCalculator.CalculatePrice();
        }

        public  List<ProductLine> GetProductLines()
        {
            return priceCalculator.GetProductLines();
        }
    }
}
